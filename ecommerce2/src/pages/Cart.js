import React,{useState, useEffect} from "react";
import { Col, Row, Button, Card } from "antd";
import ProductList from "./ProductList";
import {useSelector, useDispatch} from "react-redux";

function Cart() {
  const selectiveProduct = useSelector((state) => state);
  
  useEffect(()=>{
    console.log("selectiveProduct", selectiveProduct);
  });
  return (
    <div className="w-80">
      <Row gutter={[16, 16]}>
        <Col className="gutter-row" xs={24} sm={24} md={24} lg={18}>
          <ProductList />
        </Col>
        <Col className="gutter-row" xs={24} sm={24} md={24} lg={6}>
          <Card>
            {/* <Button size="large" block danger type="primary">Product detail here!</Button> */}
            <h2>Summary</h2>
            
            <div className="pad-5">
                <b>Total Items: {selectiveProduct.cartData.products.length}</b> 
                <br />
                <b>Total Price: ${selectiveProduct.cartData.TotalPrice}</b> 
            </div>
            <div className="pad-5">
            <Button size="large" block danger >Checkout</Button>
            </div>
            
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default Cart;
