import { ShoppingCartOutlined } from "@ant-design/icons";
import { PageHeader } from "antd";
import { Link } from "react-router-dom";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import DarkMode from "../component/Darkmode";

const Navbar = () => {
  const { TotalQuantity } = useSelector((state) => state.cartData);
  console.log("TotalQuantity", TotalQuantity);
  const gotoHome = () => {};
  return (
    <>
      <div className="site-page-header-ghost-wrapper">
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          title={
            <Link to={`/`}>
              <img
                src={require(`../images/logo.webp`)}
                height="45"
                alt="logo"
                onClick={gotoHome}
              />{" "}
            </Link>
          }
          extra={
            <>
            <DarkMode />
              <Link to="cart">
                <ShoppingCartOutlined
                  style={{ color: "#ff6e68", fontSize: "28px" }}
                />
              </Link>
              
              <div className="basket">
                <span>{TotalQuantity}</span>
              </div>
            </>
          }
        ></PageHeader>
      </div>
    </>
  );
};

export default Navbar;
