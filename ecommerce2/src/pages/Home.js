import "antd/dist/antd.css";
import { Link } from "react-router-dom";
import "../App.css";
import ProductCard from "../component/Card";
import Header from "./Header";

function Home() {
  return (
    <>
      <Header />
      <ProductCard />
    </>
  );
}

export default Home;
