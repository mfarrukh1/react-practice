import { DeleteOutlined, EyeOutlined } from "@ant-design/icons";
import { Avatar, Menu, Table, Tag } from "antd";
import React, { useState } from "react";
import EllipsisDropdown from "../component/EllipsisDropdown";
import { useSelector, useDispatch } from "react-redux";
import { inc, dec, remove } from "../Reducer/CartReducer";



const ProductList = () => {
  const columns = [
    {
      title: "Image",
      dataIndex: "image",
      key: "image",
      // render: (text) => <a>{text}</a>,
      render: (record) => (
        <div>
          <Avatar
            size={60}
            type="square"
            // src="https://emilus.themenate.net/img/thumbs/thumb-7.jpg"
            src={require(`../images/${record}`)}
            name="aslas"
          />
          {/* <b> Altas</b> */}
        </div>
      ),
    },
    {
      title: "",
      dataIndex: "name",
      key: "name",
      render: (record) => <b>{record}</b>,
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Tags",
      key: "quantity",
      dataIndex: "quantity",
      render: (quantity, record) => (
        <>
          <Tag color="geekblue" onClick={() => handleInc(record)}>
            +
          </Tag>
          <Tag color="black">{quantity}</Tag>
          <Tag color="volcano" onClick={() => handleDec(record)}>-</Tag>
        </>
      ),
    },
    {
      title: "Total Price",
      dataIndex: "Tprice",
      key: "Tprice",
      render: (_, elm) => (
        <div className="text-right">
          <em>{elm.price * elm.quantity}</em>
        </div>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (_, elm) => (
        <div className="text-right">
          <EllipsisDropdown menu={dropdownMenu(elm)} />
        </div>
      ),
    },
  ];

  const select = useSelector((state) => state.cartData.products);
  const dispatch = useDispatch();
  const handleInc = (record) => {
    dispatch(inc(record.id))
  };

  const handleDec = (record) => {
    dispatch(dec(record.id))
  };  
  const dropdownMenu = (row) => (
    <Menu>
      <Menu.Item>
        <EyeOutlined />
        <span className="ml-2">View Details</span>
      </Menu.Item>
      <Menu.Item>
        <DeleteOutlined />
        <span className="ml-2" onClick={()=> dispatch(remove(row.id))}>Delete</span>
        {/* <span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span> */}
      </Menu.Item>
    </Menu>
  );
  console.log("select", select);
  return (
    <div>
      <Table columns={columns} dataSource={select} rowKey="id" />
    </div>
  );
};

export default ProductList;
