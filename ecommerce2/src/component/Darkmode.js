import React, { useEffect, useState } from "react";

const DarkMode = () => {
  const [theme, setTheme] = useState("light-theme");


  useEffect(() => {
    document.body.className = theme;
    console.log(document.body.className);
  }, [theme]);

  const toggleTheme = () => {
    if (theme === "dark-theme") {
      setTheme("light-theme");
    } else {
      setTheme("dark-theme");
    }
  
    };

  return (
    <div className="toggle-theme-wrapper">
      <span>☀️</span>
      <label className="toggle-theme" htmlFor="checkbox">
        <input
          type="checkbox"
          id="checkbox"
          // 6
          onChange={toggleTheme}
          //   defaultChecked={defaultDark}
        />
        <div className="slider round"></div>
      </label>
      <span>🌒</span>
    </div>
  );
};

export default DarkMode;
