import { Card, Col, Row } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { productData } from "../Reducer/ProductReducer";
const { Meta } = Card;

const style = {
  background: "gray",
};

function ProductCard() {
  const { products } = useSelector((state) => state.products);
  const dispatch = useDispatch();
  console.log("home component", products);
  const getProductDetail = (data) => {
    dispatch(productData());
  };
  return (
    <div className="w-80">
      <Row gutter={[16, 16]}>
        {products.map((product) => (
          <div key={product.id}>
            <Col className="gutter-row card-bg" xs={24} sm={12} md={12} lg={6}>
              <Link to={`detail`}>
                <Card
                className="cardProd"
                  onClick={() => dispatch(productData(product))}
                  style={{
                    width: 300,
                  }}
                  cover={
                    <img
                      //   src={require(`../images/${product.image}`).default}
                      src={require(`../images/${product.image}`)}
                      alt="pic"
                      className="prod__img"
                    />
                  }
                  actions={[
                    <span>
                      <span className="line-through black">
                        ${product.price}.00
                      </span>{" "}
                      <span>{product.discount}%</span>
                    </span>,

                    <h3>${product.discountprice}</h3>,
                  ]}
                >
                  <Meta
                    //   avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                    title={`${product.name}`}
                    description="This the is the description"
                  />
                </Card>
              </Link>
            </Col>
          </div>
        ))}
      </Row>
    </div>
  );
}

export default ProductCard;
