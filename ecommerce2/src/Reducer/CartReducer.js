import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  value: 1,
  products: [],
  TotalPrice: 0,
  TotalQuantity: 0,
};

export const productCartData = createSlice({
  name: "cart",
  initialState: initialStateValue,
  reducers: {
    addToCart: (state, action) => {
      const { qty, product } = action.payload;

      //   const check = state.products.find((pr) => pr.id === product.id);

      let totalQty = state.TotalQuantity + qty;
      //   product.quantity =34;
      const objCopy = { ...product }; // 👈️ create copy
      const totalPrice = state.TotalPrice + objCopy.price * qty;
      objCopy.quantity = qty;

      return {
        ...state,
        products: [...state.products, objCopy],
        TotalQuantity: totalQty,
        TotalPrice: totalPrice,
      };
    },
    inc: (state, action) => {
      const index = JSON.parse(JSON.stringify(state.products)).findIndex(
        (pr) => pr.id === action.payload
      );
      state.products[index].quantity += 1;
      state.TotalQuantity += 1;
      state.TotalPrice += state.products[index].price;
    },

    dec: (state, action) => {
      const index = JSON.parse(JSON.stringify(state.products)).findIndex(
        (pr) => pr.id === action.payload
      );
      if (state.products[index].quantity != 1) {
        state.products[index].quantity -= 1;
        state.TotalPrice -= state.products[index].price;
      } else {
        state.TotalPrice -= state.products[index].price;
        state.products.splice(index, 1);
      }

      state.TotalQuantity -= 1;
    },
    remove: (state, action) => {
      const findpro = state.products.find(
        (product) => product.id === action.payload
      );
      const filtered = state.products.filter(
        (product) => product.id !== action.payload
      );
      return {
        ...state,
        products: filtered,
        TotalPrice: state.TotalPrice - findpro.price * findpro.quantity,
        TotalQuantity: state.TotalQuantity - findpro.quantity,
      };
    },
  },
});

export const { addToCart, inc, dec, remove } = productCartData.actions;
export default productCartData.reducer;
