import { configureStore } from "@reduxjs/toolkit";
import ProductReducer from "./Reducer/ProductReducer";
import CartReducer from "./Reducer/CartReducer";

const store = configureStore({
  reducer: {
    products: ProductReducer,
    cartData: CartReducer,
  },
});

export default store;
